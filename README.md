# quovadis

REST API to assist with ACME challenges

## API

### Documentation

GET / 

### Register User

POST /register

Params:
* UCC username

Will email to your UCC email address an API key to use with the service

### Update API key

POST /update-api-key

Params:
* UCC username
* Old API key
* New API key

Will update API key

Will email to your UCC email address noting that the API key has been updated

### Update Challenge

POST /update-challenge

Params:

* UCC username
* API key
* Challenge Handle
* Challenge TXT

Will update the DNS TXT record at 

<ucc username>-<challenge handle>.quovadis.ucc.asn.au 

to the contents of Challenge TXT


## Use

This is intended to be used with the certbot hook scripts included alongside this code

register-apikey.sh

certbot-hook-quovadis.sh


## Author

Mark Tearle <mtearle@ucc.asn.au>

<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once('config.php');
use Ramsey\Uuid\Uuid;
use RestService\RestService;
use GuzzleHttp\Client;
use Leaf\Http\Response;

function check_username($username) {
	// returns true if we get some info about the username
	return is_array(posix_getpwnam($username));
}

/* from https://stackoverflow.com/questions/1755144/how-to-validate-domain-name-in-php/48801316 */

function is_valid_domain_name($domain_name)
{
    return (preg_match("/^([a-z\d](-*[a-z\d])*)(\.([a-z\d](-*[a-z\d])*))*$/i", $domain_name) //valid chars check
            && preg_match("/^.{1,253}$/", $domain_name) //overall length check
            && preg_match("/^[^\.]{1,63}(\.[^\.]{1,63})*$/", $domain_name)   ); //length of each label
}

/**
	*   Remove the first and last quote from a quoted string of text
	*  
	*   From https://stackoverflow.com/questions/9734758/remove-quotes-from-start-and-end-of-string-in-php/25353877#25353877
	*   @param mixed $text
	*     */
function stripQuotes($text) {
	  return preg_replace('/^(\'(.*)\'|"(.*)")$/', '$2$3', $text);
} 

function get_txt($config, $name) {
	return get_rfc2136_txt($config, $name);
}

function get_rfc2136_txt($config, $name) {

	//
        // create new resolver object, passing in an array of name
	// servers to use for lookups
        //
        $r = new Net_DNS2_Resolver(array('nameservers' => array($config['QV_RFC2136_HOST'])));

	//
	// execute the query request for the name TXT
	//
	try {
		$result = $r->query($name . "." . $config['QV_DOMAIN'], 'TXT');

	} catch(Net_DNS2_Exception $e) {

		#echo "::query() failed: ", $e->getMessage(), "\n";
		return "";
	}

	//
	// loop through the answer, printing out the TXT results returned.
	//
	foreach($result->answer as $txtrr)
	{
		$answer = $txtrr->asArray();
		$txt = trim(end($answer),'"');
		return $txt;
	}
	//

	return "";
}

function get_dynu_domainid($config, $name) {
	$restService = new RestService();
	$path = "/v2/dns/getroot/" . $name . "." . $config['QV_DOMAIN'];
	$auth_string = $config['QV_DYNU_API'];
	
	$response = $restService
		->setEndpoint('https://api.dynu.com')
		->setRequestHeaders([
			'API-Key' => $auth_string
			# accept: application/json
			])
		->get($path, [], [], false);

	$result = json_decode($response->getBody(), true);
	var_dump($result);
	if (array_key_exists('id', $result)) {
	    return $result['id'];
	} 

	return 0;
}

function get_dynu_txt($config, $name) {
	$restService = new RestService();
	$path = "/v2/dns/record/" . $name . "." . $config['QV_DOMAIN'];
	$query = [
	   "recordType"=>"TXT",
	];
	$auth_string = $config['QV_DYNU_API'];
	
	$response = $restService
		->setEndpoint('https://api.dynu.com')
		->setRequestHeaders([
			'API-Key' => $auth_string
			# accept: application/json
			])
		->get($path, $query, [], false);

	$result = json_decode($response->getBody(), true);
	var_dump($result);
	if (array_key_exists('dnsRecords', $result)) {
	    if (count($result['dnsRecords'])){
	        if (array_key_exists('textData', $result['dnsRecords'][0])) {
		    return stripQuotes($result['dnsRecords'][0]['textData']);
	        }
	    } 
	} 

	return "";
}

function get_dynu_txt_recordid($config, $name) {
	$restService = new RestService();
	$path = "/v2/dns/record/" . $name . "." . $config['QV_DOMAIN'];
	$query = [
	   "recordType"=>"TXT",
	];
	$auth_string = $config['QV_DYNU_API'];
	
	$response = $restService
		->setEndpoint('https://api.dynu.com')
		->setRequestHeaders([
			'API-Key' => $auth_string
			# accept: application/json
			])
		->get($path, $query, [], false);

	$result = json_decode($response->getBody(), true);
	var_dump($result);
	if (array_key_exists('dnsRecords', $result)) {
	    if (count($result['dnsRecords'])){
	        if (array_key_exists('id', $result['dnsRecords'][0])) {
		    return stripQuotes($result['dnsRecords'][0]['id']);
	        }
	    } 
	} 

	return 0;
}

function get_desec_txt($config, $name) {
	$restService = new RestService();
	$path = "/api/v1/domains/" . $config['QV_DOMAIN'] . "/rrsets/" . $name . "/TXT/";
	$auth_string = "Token ". $config['QV_DESEC_API'];

	try {	
		$response = $restService
			->setEndpoint('https://desec.io')
			->setRequestHeaders([
				'Authorization' => $auth_string
				])
			->get($path, [], [], false);
	} catch (Exception $e) {
		if( $e->getMessage() == "Not Found" ) {
			return "";
		}
        	throw new Exception($e->getMessage(), $e->getCode());
        }

	$result = json_decode($response->getBody(), true);
	if (array_key_exists('records', $result)) {
		return stripQuotes($result['records'][0]);
	} else {
		return "";
	}
}

function encrypt_api_key($api_key) {
	return bin2hex(password_hash($api_key, PASSWORD_DEFAULT));
}

function check_api_key($config, $username, $api_key) {
	// API keys need to be alpha numeric
        $hash = hex2bin(get_txt($config, $username));
	return password_verify($api_key, $hash);
}

function check_challenge($challenge) {
	// Challenge needs to be alpha numeric (meet DNS requirements)
	return 1;
}

// methods to alter and update desec.io

//
// curl -X PUT https://desec.io/api/v1/domains/{name}/rrsets/{subname}/{type}/ \
//    --header "Authorization: Token {token}" \
//    --header "Content-Type: application/json" --data @- <<EOF
//    {
//      "subname": "{subname}",
//      "type": "{type}",
//      "ttl": 3600,
//      "records": ["..."]
//    }
// EOF
//

function update_txt($config, $name, $txt) {
        return update_rfc2136_txt($config, $name, $txt);
}

function delete_rfc2136_txt($config, $name) {
	// create a new Updater object
	//
	$u = new Net_DNS2_Updater($config['QV_DOMAIN'], array('nameservers' => array($config['QV_RFC2136_HOST'])));

	try {
		$u->deleteAny($name . "." . $config['QV_DOMAIN'], 'TXT');
		
		// add a TSIG to authenticate the request
		//
		$u->signTSIG($config['QV_RFC2136_KEYNAME'], $config['QV_RFC2136_KEY']);

		//
		// execute the request
		//
		$u->update();

		return "$name deleted";

	} catch(Net_DNS2_Exception $e) {

		#echo "::update() for deleteAny failed: ", $e->getMessage(), "\n";
	}

	return "";
}

function update_rfc2136_txt($config, $name, $txt) {

	delete_rfc2136_txt($config, $name);

	// create a new Updater object
	//
	$u = new Net_DNS2_Updater($config['QV_DOMAIN'], array('nameservers' => array($config['QV_RFC2136_HOST'])));

	try {
		//
		// create a new MX RR object to add to the example.com zone
		//
		$txtrr = Net_DNS2_RR::fromString($name . "." . $config['QV_DOMAIN'] . ' TXT "'. $txt . '"');

		$txtrr->ttl=60;

		//
		// add the record
		//
		$u->add($txtrr);

		// add a TSIG to authenticate the request
		//
		$u->signTSIG($config['QV_RFC2136_KEYNAME'], $config['QV_RFC2136_KEY']);

		//
		// execute the request
		//
		$u->update();

		return $txt;

	} catch(Net_DNS2_Exception $e) {

		#echo "::update() failed: ", $e->getMessage(), "\n";
	}

	return "";
}

function update_dynu_txt($config, $name, $txt) {
	$domainId = get_dynu_domainid($config, $name);

	# delete dynu txt
	$recordid = get_dynu_txt_recordid($config, $name);

	while ($recordid) {
             delete_dynu_record($config, $domainId, $recordid);	
	     $recordid = get_dynu_txt_recordid($config, $name);
	}
	 
	# create dynu txt
	# curl -s -X POST "https://api.dynu.com/v2/dns/$domainID/record" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"nodeName\":\"_acme-challenge\",\"recordType\":\"TXT\",\"ttl\":60,\"state\":true,\"group\":\"\",\"textData\":\"$CERTBOT_VALIDATION\"}" -H "API-Key: $api_key"

	$path = "/v2/dns/" . $domainId . "/record/" . $recordid;
	$auth_string = $config['QV_DYNU_API'];

	$headers = [
		'API-Key' => $auth_string,
		'Content-Type' => "application/json",
	];


	$val =	[
			'nodeName' => $name,
			'recordType' => 'TXT',
			'ttl' => 120,
			'state' => true,
			'group' => '',
			'textData' => $txt,
		];

	$body =  json_encode($val);


	$client = new GuzzleHttp\Client([
		'base_uri' => 'https://api.dynu.com',
		'timeout' => 2, 
		'debug' => true
	]);

        try {
	    $response = $client->request('POST', $path, [ 'headers'=>$headers, 'body'=>$body ]);
        } catch (ClientException $e) {
            throw new Exception($e->getResponse()->getReasonPhrase(), $e->getResponse()->getStatusCode());
        } catch (BadResponseException $e) {
            throw new Exception($e->getResponse()->getReasonPhrase(), $e->getResponse()->getStatusCode());
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }

	$result = json_decode($response->getBody(), true);

	return "";

}

function delete_dynu_record($config, $domainId, $recordid) {
	# curl -X DELETE "https://api.dynu.com/v2/dns/100000000/record/2" -H "accept: application/json"
	$restService = new RestService();
	$path = "/v2/dns/" . $domainId . "/record/" . $recordid;
	$auth_string = $config['QV_DYNU_API'];
	$response = $restService
		->setEndpoint('https://api.dynu.com')
		->setRequestHeaders([
			'API-Key' => $auth_string
			# accept: application/json
			])
		->delete($path, [], [], false);

	$result = json_decode($response->getBody(), true);

	return 0;
}

#
function update_desec_txt($config, $name, $txt) {
	$auth_string = "Token ". $config['QV_DESEC_API'];
	$headers = [
		'Authorization' => $auth_string,
		'Content-Type' => "application/json"

	];

	if (get_desec_txt($config, $name) == "") {
		$method = 'POST';
		$path = "/api/v1/domains/" . $config['QV_DOMAIN'] . "/rrsets/";
	} else {
		$method = 'PUT';
		$path = "/api/v1/domains/" . $config['QV_DOMAIN'] . "/rrsets/" . $name . "/TXT/";
	}

	$val =	[
			'subname' => $name,
			'type' => 'TXT',
			'ttl' => 60,
			'records' => ['"'. $txt . '"',],
		];

	$body =  json_encode($val);


	$client = new GuzzleHttp\Client([
#		'debug' => true,
		'base_uri' => 'https://desec.io',
		'timeout' => 2 
	]);

        try {
	    $response = $client->request($method, $path, [ 'headers'=>$headers, 'body'=>$body ]);
        } catch (ClientException $e) {
            throw new Exception($e->getResponse()->getReasonPhrase(), $e->getResponse()->getStatusCode());
        } catch (BadResponseException $e) {
            throw new Exception($e->getResponse()->getReasonPhrase(), $e->getResponse()->getStatusCode());
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }

	$result = json_decode($response->getBody(), true);
	if (array_key_exists('records', $result)) {
		return stripQuotes($result['records'][0]);
	} else {
		return "";
	}
}

// Send email to user with API key

function email_api_key($username, $api_key) {
    $headers = array(
        'From' => 'quovadis@ucc.asn.au',
        'Reply-To' => 'wheel@ucc.asn.au',
        'X-Mailer' => 'PHP/' . phpversion()
    );

    // The message
    $message = <<<END
Quovadis

UCC DNS Helper

Your API key is: $api_key

Rough API usage is:

curl -X POST -d username=accmurphy https://quovadis.ucc.asn.au/quovadis/register
curl -X POST -d username=accmurphy -d api_key=foo -d new_api_key=bar https://quovadis.ucc.asn.au/quovadis/update-api-key
curl -X POST -d username=accmurphy -d api_key=foo -d challenge=thingy -d value=123456abcdef https://quovadis.ucc.asn.au/quovadis/update-challenge

This API can be used by a certbot hook script. 

certbot certonly --server https://acme-v02.api.letsencrypt.org/directory \
        --manual \
	--preferred-challenges=dns \
        --manual-auth-hook /etc/letsencrypt/ucc-hooks/certbot-hook-quovadis.sh \
        -d thehostname.ucc.asn.au

The hook script can be downloaded from:

* https://gitlab.ucc.asn.au/UCC/quovadis/-/blob/master/scripts/certbot-hook-quovadis.sh
* https://gitlab.ucc.asn.au/UCC/quovadis/-/blob/master/scripts/config.sh
END;

    // In case any of our lines are larger than 70 characters, we should use wordwrap()
    $message = wordwrap($message, 70, "\r\n");

    // Send
    mail($username . '@ucc.asn.au', 'Quoavadis API Key', $message, $headers);
}

// Instantiate Leaf
$leaf = new Leaf\App();
#$response = new Leaf\Http\Response();

// Add routes
$leaf->get('/', function () use($leaf) {
   $config=get_config();
   // since the response object is directly tied to the leaf instance
   $html = <<<END
<h1>Quovadis</h1>

<h2>UCC DNS Helper</h2>

<p>
Rough API usage is:
</p>

<pre>
curl -X POST -d username=accmurphy https://quovadis.ucc.asn.au/quovadis/register
curl -X POST -d username=accmurphy -d api_key=foo -d new_api_key=bar https://quovadis.ucc.asn.au/quovadis/update-api-key
curl -X POST -d username=accmurphy -d api_key=foo -d challenge=thingy -d value=123456abcdef https://quovadis.ucc.asn.au/quovadis/update-challenge
</pre>

<p>
This API can be used by a certbot hook script. 
</p>

<pre>
certbot certonly --server https://acme-v02.api.letsencrypt.org/directory \
        --manual \
	--preferred-challenges=dns \
        --manual-auth-hook /etc/letsencrypt/ucc-hooks/certbot-hook-quovadis.sh \
        -d thehostname.ucc.asn.au
</pre>

<p>
The hook script can be downloaded from:
</p>

<ul>
<li>
<a href="https://gitlab.ucc.asn.au/UCC/quovadis/-/blob/master/scripts/certbot-hook-quovadis.sh">
https://gitlab.ucc.asn.au/UCC/quovadis/-/blob/master/scripts/certbot-hook-quovadis.sh
</a>
</li>
<li>
<a href="https://gitlab.ucc.asn.au/UCC/quovadis/-/blob/master/scripts/config.sh">
https://gitlab.ucc.asn.au/UCC/quovadis/-/blob/master/scripts/config.sh
</a>
</li>
</ul>
END;

   $leaf->response()->markup($html);
});

$leaf->post('/register', function () use($leaf) {
// 
// ### Register User
// 
// POST /register
// 
// Params:
// * UCC username
// 
// Will email to your UCC email address an API key to use with the service
    $config=get_config();

    $username = $leaf->request->get('username');

    // Check username is valid
    if ( !check_username($username) ) {
        $leaf->response->json(["message" => $username." not valid"],200);
	return;
    }

    // Check there is no existing API key registered for the user
    $a = get_txt($config, $username);
    if ( $a <> "" ) {
        $leaf->response->json(["message" => $username." already registered"],200);
	return;
    }

    // Generate API key
    $api_key = Uuid::uuid4()->toString();
    
    // Stick API key encrypted into the DNS
    $u = update_txt($config, $username, encrypt_api_key($api_key));
    
    // Email API key
    email_api_key($username, $api_key);

    // Don't echo API key back as sending it by email to the UCC member stops
    // folks hitting the API and being able to cause havoc 
    $leaf->response->json(["message" => "API key for " .$username." has been added and email sent"],200);
});

$leaf->post('/update-api-key', function () use($leaf) {
// 
// ### Update API key
// 
// POST /update-api-key
// 
// Params:
// * UCC username
// * Old API key
// * New API key
// 
// Will update API key
// 
// Will email to your UCC email address noting that the API key has been updated
    $config=get_config();

    $username = $leaf->request->get('username');
    $api_key = $leaf->request->get('api_key');
    $new_api_key = $leaf->request->get('new_api_key');
    
    // Check username is valid
    if ( !check_username($username) ) {
        $leaf->response->json(["message" => $username." not valid"],200);
	return;
    }

    // Check API key
    if ( !check_api_key($config, $username, $api_key) ) {
        $leaf->response->json(["message" => "API key for " .$username." not valid"],200);
	return;
    }
    
    
    // Encrypt API key
    // Stick API key encrypted into the DNS
    $u = update_txt($config, $username, encrypt_api_key($new_api_key));
    
    
    $leaf->response->json(["message" => $username." API key has been updated"],200);
    
    // Email user to let them know API key has updated
    // Email API key
    email_api_key($username, "(REDACTED, set by user)");
});

$leaf->post('/update-challenge', function () use($leaf) {
// 
// ### Update Challenge
// 
// POST /update-challenge
// 
// Params:
// 
// * UCC username
// * API key
// * Challenge Handle
// * Challenge TXT
// 
// Will update the DNS TXT record at 
// 
// <ucc username>-<challenge handle>.quovadis.ucc.asn.au 
// 
// to the contents of Challenge TXT
// 
    $config=get_config();

    $username = $leaf->request->get('username');
    $api_key = $leaf->request->get('api_key');
    $challenge = $leaf->request->get('challenge');
    $value = $leaf->request->get('value');

    // Check username is valid
    if ( !check_username($username) ) {
        $leaf->response->json(["message" => $username." not valid"],200);
	return;
    }

    // Check API key
    if ( !check_api_key($config, $username, $api_key) ) {
        $leaf->response->json(["message" => "API key for " .$username." not valid"],200);
	return;
    }

    // Sanity check challenge text
    //
    $handle = $username . "-" . $challenge;
    if ( !is_valid_domain_name($handle) ) {
        $leaf->response->json(["message" => "Handle " .$handle." not valid"],200);
	return;
    }

    // Stick challenge value prefixed by username into DNS
    $u = update_txt($config, $handle, $value);
    
    $leaf->response->json(["message" => $handle." has been updated"],200);
    
});

$leaf->run();

?>

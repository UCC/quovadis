#!/bin/bash

# certbot-hook-quovadis.sh

# INSTALL:

#  mkdir mkdir /etc/letsencrypt/ucc-hooks
#  cp certbot-hook-quovadis.sh /etc/letsencrypt/ucc-hooks

# CONFIG:

# Create a config file config.sh in the same directory as the script
# containing

#   QV_API_KEY=<your API key>
#   UCC_USERNAME=<your UCC username>

# USAGE:
# Script runs as a manual auth hook with certbot

# Example command line is:

# certbot certonly --server https://acme-v02.api.letsencrypt.org/directory \
#                  --manual \
#                  --preferred-challenges=dns 
#                  --manual-auth-hook /etc/letsencrypt/ucc-hooks/certbot-hook-quovadis.sh \
#                  -d <the hostname>.ucc.asn.au


SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  # if $SOURCE was a relative symlink, we need to resolve it relative 
  # to the path where the symlink file was located
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" 
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

if [ -f "$DIR/config.sh" ]; then
	source $DIR/config.sh
else
	echo "config.sh not found in same directory as script"
fi


# Strip only the top domain to get the zone id
DOMAIN=$(expr match "$CERTBOT_DOMAIN" '\(.*\)\.ucc\..*')

if [ -z "${DOMAIN}" ]; then
        DOMAIN="apex"
fi

BASE_API="${BASE_API:-https://quovadis.ucc.asn.au/quovadis}"

# Create TXT record
RECORD_ID=$(curl -s -X POST "$BASE_API/update-challenge" \
	-d "username=$UCC_USERNAME" \
	-d "api_key=$QV_API_KEY" \
	-d "challenge=$DOMAIN" \
	-d "value=$CERTBOT_VALIDATION")

# Sleep to make sure the change has time to propagate over to DNS
sleep 25
